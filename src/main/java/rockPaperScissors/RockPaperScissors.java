package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // Game loop
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            // Human and Computer choice
            String humanChoiceStr = userChoice();
            String computerChoiceStr = randomChoice();
            String choiceString = "Human chose " + humanChoiceStr + ", computer chose " + computerChoiceStr;
            boolean humanWinBool = isWinner(humanChoiceStr, computerChoiceStr);
            boolean computerWinBool = isWinner(computerChoiceStr, humanChoiceStr);

            // Check who won
            if (humanWinBool) {
                System.out.println(choiceString + " Human wins!");
                humanScore++;
            } else if (computerWinBool) {
                System.out.println(choiceString + " Computer wins!");
                computerScore++;
            } else {
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human " + humanScore+ ", computer " + computerScore);

            // Ask if human wants to play again
            boolean stopAnswerBool = stopPlaying();
            roundCounter++;
            if (stopAnswerBool) {
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Chooses random out of rock paper scissors
     * @return random choice
     */
    public String randomChoice() {
        Random rand = new Random(); //instance of random
        int intRandom = rand.nextInt(3);
        String choice = rpsChoices.get(intRandom);
        return choice;
    }

    /**
     * Cheks if choice1 is wins over choice2
     * @param choice1
     * @param choice2
     * @return true if choice1 beats choice2, false if not
    */
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }

    /**
     * Prompt the user with what choice of rock, paper, scissors they choose
     * @return "rock", "paper" or "scissors"
     */
    public String userChoice() {
        while (true) {
            String userChoiceStr = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validateInput(userChoiceStr, rpsChoices)) {
                return userChoiceStr;
            } else {
                System.out.println("I do not understand " + userChoiceStr + ". Could you try again?");
            }
        }
    }
    
    /**
     * Prompts the user if they want to continue playing
     * @return true if answer is "n"
     */
    public boolean stopPlaying() {
        List<String> ynChoicesList = Arrays.asList("y", "n");
        while (true) {
            String ynAnswerStr = readInput("Do you wish to continue playing? (y/n)?");
            if (validateInput(ynAnswerStr, ynChoicesList)) {
                return ynAnswerStr.equals("n");
            } else {
                System.out.println("I do not understand " + ynAnswerStr + ". Could you try again?");
            }
        }
    }
    
    /**
     * Checks if the given input is either rock, paper or scissors
     * @param input user input string
     * @param validInput list of valid input strings
     * @return true if valid input, false otherwise
     */
    public boolean validateInput(String input, List<String> validInput) {
        return validInput.contains(input);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
